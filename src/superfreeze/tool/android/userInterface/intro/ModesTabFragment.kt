/*
 * Copyright (c) 2019 Hocuri
 * Copyright (c) 2019 Robin Naumann
 *
 * This file is part of SuperFreezZ.
 *
 * SuperFreezZ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SuperFreezZ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SuperFreezZ.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package superfreeze.tool.android.userInterface.intro

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import superfreeze.tool.android.R

@SuppressLint("Registered")
/**
 * Class for the second intro page, explaining the freeze modes
 */
class ModesTabFragment : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_intro_modes)

        val toolbar = findViewById<Toolbar>(R.id.modes_toolbar)
        val tabLayout = findViewById<TabLayout>(R.id.modes_tabLayout)
        val viewPager2 = findViewById<ViewPager2>(R.id.modes_viewPager2)

        if (toolbar != null) {
            setSupportActionBar(toolbar)
        }

        val adapter = ModesStateAdapter(this)
        viewPager2.adapter = adapter
        TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
            tab.text = "Nothing for now"
            //tab.text = getString(adapter.getPageTitle(position))
        }.attach()
    }

    private class ModesStateAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

        override fun createFragment(position: Int): Fragment {
            return Fragment()
        }

        override fun getItemCount(): Int {
            return 1
        }
/*
        fun getPageTitle(position: Int): Int {
            return when (position) {
                0 -> R.string.tab_text_1
                1 -> R.string.tab_text_2
                else -> throw IllegalArgumentException("Unknown position for ViewPager2")
            }
        }
*/
    }

}